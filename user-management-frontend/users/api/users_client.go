package api

/*High level function to communicate with Backend.*/

func ResetPassword(token, pass string) (string, error) {
	data := struct {
		Token string `json:"token"`
		Pass  string `json:"pass"`
	}{Token: token, Pass: pass}

	return post("/users/reset", &data)
}

func CreateUser(email, pass string) (string, error) {

	user := struct {
		Email string `json:"email"`
		Pass  string `json:"pass"`
	}{email, pass}

	return post("/users/create", &user)
}

func ExistUser(email string) (string, error) {
	return get("/users/exists", email)
}

func AuthenticateUser(email, pass string) (string, error) {
	return get("/users/login", email, pass)
}

func CreateToken(email string) (string, error) {
	return get("/users/token", email)
}

type UserInfo struct {
	Id        int64  `json:"id"`
	Email     string `json:"email"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Cellphone string `json:"cellphone"`
	Address   string `json:"address"`
}

func GetUserInfo(email string) (*UserInfo, error) {
	var userInfo UserInfo
	v, err := getWith(&userInfo, "/users/info", email)
	return v.(*UserInfo), err
}

func UpdateUserInfo(id int64, email, firstName, lastName, cellphone, address string) (string, error) {
	var userInfo = UserInfo{Email: email, FirstName: firstName, LastName: lastName,
		Cellphone: cellphone, Id: id, Address: address}
	return post("/users/update", &userInfo)
}
