package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

/*Low level http operations to communicate with backend.*/

var endpoint string

type basicResponse struct {
	Message string `json:"message"`
}

func ConfigureEndpoint(url string) {
	endpoint = url
}

func post(path string, v interface{}) (string, error) {
	json, err := json.Marshal(&v)
	if err != nil {
		return "Error creating json values", err
	}
	return handleResponse(http.Post(endpoint+path, "application/json", bytes.NewBuffer(json)))
}

func get(path string, parameters ...string) (string, error) {
	return handleResponse(http.Get(createUrlPath(path, parameters...)))
}

func createUrlPath(path string, parameters ...string) string {
	url := endpoint + path
	for _, param := range parameters {
		url = fmt.Sprintf("%s/%s", url, param)
	}
	return url
}

func handleResponse(response *http.Response, err error) (string, error) {
	v, err := handleResponseWith(&basicResponse{}, response, err)
	return v.(*basicResponse).Message, err
}

func handleResponseWith(v interface{}, response *http.Response, err error) (interface{}, error) {
	if err != nil {
		return "Error getting server response", err
	}

	//Fix: Always close stream.
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "Error reading server response", nil
	}

	err = json.Unmarshal(body, v)
	if err != nil {
		return "Error reading server response when doing unmarshalling", err
	}

	if response.StatusCode == http.StatusOK {
		return v, nil
	}

	return v, ErrFailedRequest("Failed request")
}

func getWith(v interface{}, path string, parameters ...string) (interface{}, error) {
	resp, err := http.Get(createUrlPath(path, parameters...))
	return handleResponseWith(v, resp, err)
}

type ErrFailedRequest string

func (e ErrFailedRequest) Error() string {
	return string(e)
}

func IsFailedRequest(err error) bool {
	_, ok := err.(ErrFailedRequest)
	return ok
}
