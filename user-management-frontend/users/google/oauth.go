package google

import (
	"crypto/rand"
	"encoding/base64"
	"log"
	"os"

	"context"

	"user-management-frontend/users/api"

	"io/ioutil"

	"encoding/json"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var frontendUrl, secret, clientId string

type User struct {
	Sub           string `json:"sub"`
	Name          string `json:"name"`
	GivenName     string `json:"given_name"`
	FamilyName    string `json:"family_name"`
	Profile       string `json:"profile"`
	Picture       string `json:"picture"`
	Email         string `json:"email"`
	EmailVerified bool   `json:"email_verified"`
	Gender        string `json:"gender"`
}

var oauthCredentials *oauth2.Config

func EnsureGoogleOAuthVars() {
	frontendUrl = os.Getenv("FRONTEND_URL")
	if frontendUrl == "" {
		log.Fatal("I can't get FRONTEND_URL")
	}
	clientId = os.Getenv("CLIENT_ID")
	if clientId == "" {
		log.Fatal("I can't get CLIENT_ID")
	}
	secret = os.Getenv("SECRET")
	if secret == "" {
		log.Fatal("I can't get SECRET")
	}

	oauthCredentials = &oauth2.Config{
		RedirectURL:  frontendUrl + "/public/googlecallback",
		ClientID:     clientId,
		ClientSecret: secret,
		Endpoint:     google.Endpoint,
		Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"}}
}

func MakeRedirectURL(token string) string {
	return oauthCredentials.AuthCodeURL(token)
}

func AccessToken(code string) (*oauth2.Token, error) {
	return oauthCredentials.Exchange(context.Background(), code)
}

func CreateUserInfo(accessToken *oauth2.Token) (*api.UserInfo, error) {
	client := oauth2.NewClient(context.Background(), oauth2.StaticTokenSource(accessToken))
	resp, err := client.Get("https://www.googleapis.com/oauth2/v3/userinfo")
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var user User
	err = json.Unmarshal(body, &user)
	if err != nil {
		return nil, err
	}

	return &api.UserInfo{Address: "", Cellphone: "", Email: user.Email, FirstName: user.Name, LastName: user.FamilyName}, nil

}

//This should be unique so a verification mechanism should exists. For fast development I can't implement it yet.
func RandToken() string {
	b := make([]byte, 32)
	rand.Read(b)
	return base64.StdEncoding.EncodeToString(b)
}
