package tmpl

import (
	"html/template"
	"io"
)

//TemplateProvider is responsible to find a template for a given template name.
type TemplateProvider interface {
	FindTemplate(name string) *template.Template
}

//Renderer is responsible to write all html templates.
type Renderer interface {
	Render(string, interface{}, io.Writer)
}

type defaultRenderer struct {
	provider TemplateProvider
}

var (
	renderer Renderer
	_        Renderer = &defaultRenderer{}
)

func InitializeRenderer(provider TemplateProvider) {
	dr := new(defaultRenderer)
	dr.provider = provider
	renderer = dr

}

func (dr *defaultRenderer) Render(name string, data interface{}, w io.Writer) {
	template := dr.provider.FindTemplate(name)
	template.Execute(w, data)
}

func RenderWith(name string, templateData *TemplateData, w io.Writer) {
	renderer.Render(name, templateData, w)
}

func Render(name string, w io.Writer) {
	renderer.Render(name, nil, w)
}

type TemplateData struct {
	Data   map[string]interface{}
	Errors []string
	Infos  []string
}

func NewTD() *TemplateData {
	td := new(TemplateData)
	td.Errors = make([]string, 0, 5)
	td.Infos = make([]string, 0, 2)
	td.Data = make(map[string]interface{})
	return td
}

func (td *TemplateData) Add(error string) {
	td.Errors = append(td.Errors, error)
}

func (td *TemplateData) Info(error string) {
	td.Infos = append(td.Infos, error)
}

func (td *TemplateData) HasErrors() bool {
	return len(td.Errors) > 0
}
