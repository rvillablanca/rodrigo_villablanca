package tmpl

import (
	"fmt"
	"html/template"
)

/*Default Template Provider*/


type DefaultTemplateProvider struct {
	templates map[string]*template.Template
}

func (t *DefaultTemplateProvider) FindTemplate(name string) *template.Template {
	tmpl, ok := t.templates[name]
	if !ok {
		//Panic if a template is not found. Using a non existent template is a developer error.
		panic(fmt.Sprintf("Template %s was not found", name))
	}
	return tmpl
}

func (t *DefaultTemplateProvider) AddTemplate(name string, files ...string) {
	tmpl := template.Must(template.ParseFiles(files...))
	t.templates[name] = tmpl
}

func NewDefaultTemplateProvider() *DefaultTemplateProvider {
	t := new(DefaultTemplateProvider)
	t.templates = make(map[string]*template.Template)
	return t
}

var _ TemplateProvider = &DefaultTemplateProvider{}
