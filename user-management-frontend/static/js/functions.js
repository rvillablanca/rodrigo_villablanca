$(function () {
    closeables();
});

function closeables() {
    $('.message .close').on('click', function () {
        $(this).closest('.message').transition('fade');
    });
}