package handlers

import (
	"log"
	"net/http"
	"user-management-frontend/tmpl"
	"user-management-frontend/users/api"

	"fmt"

	"user-management-frontend/users/google"

	"github.com/gorilla/mux"
)

/*Handlers definition*/

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	tmpl.Render("login", w)
}

func CreateUserHandler(w http.ResponseWriter, r *http.Request) {
	td := td()
	email := getParam("email", r)
	if email == "" {
		td.Add("Email is required")
	}
	pass := getParam("pass", r)
	if pass == "" {
		td.Add("Pass is required")
	}

	pass2 := getParam("pass2", r)
	if pass2 == "" {
		td.Add("Pass2 is required")
	}

	if td.HasErrors() {
		tmpl.RenderWith("login", td, w)
		return
	}

	if pass != pass2 {
		td.Add("You have entered different passwords")
		tmpl.RenderWith("login", td, w)
		return
	}

	resp, err := api.CreateUser(email, pass)
	if err != nil {
		td.Add(resp + " - " + err.Error())
		tmpl.RenderWith("login", td, w)
		return
	}

	td.Data["message"] = resp
	tmpl.RenderWith("messages", td, w)
}

func ResetHandler(w http.ResponseWriter, r *http.Request) {
	td := td()
	pass := getParam("pass", r)
	if pass == "" {
		td.Add("Password is required")
	}

	pass2 := getParam("pass2", r)
	if pass2 == "" {
		td.Add("Pass2 is required")
	}

	token := getParam("token", r)
	if token == "" {
		td.Add("Missing token")
	}

	td.Data["token"] = token

	if td.HasErrors() {
		tmpl.RenderWith("recover", td, w)
		return
	}

	if pass != pass2 {
		td.Add("You have entered different passwords")
		tmpl.RenderWith("recover", td, w)
		return
	}

	resp, err := api.ResetPassword(token, pass)
	if err != nil {
		td.Add(resp + " - " + err.Error())
		tmpl.RenderWith("recover", td, w)
		return
	}

	td.Data["message"] = resp
	tmpl.RenderWith("messages", td, w)
}

func RecoveryHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	token := vars["token"]
	td := td()
	td.Data = make(map[string]interface{})
	td.Data["token"] = token
	tmpl.RenderWith("recover", td, w)
}

func TokenHandler(w http.ResponseWriter, r *http.Request) {
	td := td()
	email := getParam("email", r)
	if email == "" {
		td.Add("Email is required")
	}
	if td.HasErrors() {
		tmpl.RenderWith("login", td, w)
		return
	}

	resp, err := api.ExistUser(email)
	if err != nil {
		td.Add(resp + " - " + err.Error())
		tmpl.RenderWith("login", td, w)
		return
	}

	resp, err = api.CreateToken(email)
	if err != nil {
		td.Add(resp + " - " + err.Error())
		tmpl.RenderWith("login", td, w)
		return
	}

	td.Data["message"] = resp
	tmpl.RenderWith("messages", td, w)

}

func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	invalidateSession(w, r)
	redirect(w, r, "/public/")
}

func AuthHandler(w http.ResponseWriter, r *http.Request) {
	td := td()
	email := getParam("email", r)
	if email == "" {
		td.Add("Email is required")
	}
	pass := getParam("pass", r)
	if pass == "" {
		td.Add("Pass is required")
	}

	if td.HasErrors() {
		tmpl.RenderWith("login", td, w)
		return
	}

	//	Here, email and password strength should be validated, but I don't have time :(

	response, err := api.AuthenticateUser(email, pass)
	if err != nil {
		td.Add(response + " - " + err.Error())
		tmpl.RenderWith("login", td, w)
		return
	}

	log.Printf("Response from server: %s", response)

	userInfo, err := api.GetUserInfo(email)
	if err != nil {
		td.Add("Error getting user information - " + err.Error())
		tmpl.RenderWith("login", td, w)
		return
	}

	save("info", userInfo, r)
	save("editable", true, r)
	saveSession(w, r)

	//Mark user as authenticated
	authenticated(w, r)
	redirect(w, r, "/user/profile")
}

func ProfileHandler(w http.ResponseWriter, r *http.Request) {
	td := td()
	session := getSession(r)
	td.Data["info"] = session.Values["info"]
	td.Data["editable"] = session.Values["editable"]
	tmpl.RenderWith("profile", td, w)
}

func UpdateInfoHandler(w http.ResponseWriter, r *http.Request) {
	if isGoogleLogged(r) {
		td := td()
		td.Data["message"] = "Not allowed to save user info. This is a Google account"
		tmpl.RenderWith("messages", td, w)
		return
	}

	td := td()
	session := getSession(r)
	td.Data["info"] = session.Values["info"]
	td.Data["editable"] = session.Values["editable"]
	userInfo := session.Values["info"].(*api.UserInfo)
	email := getParam("email", r)
	if email == "" {
		td.Add("Email is required")
	}

	if td.HasErrors() {
		tmpl.RenderWith("profile", td, w)
		return
	}

	firstName := getParam("firstname", r)
	lastName := getParam("lastname", r)
	cellphone := getParam("cellphone", r)
	address := getParam("address", r)

	if userInfo.Email != email {
		_, err := api.ExistUser(email)
		if err == nil {
			//	cannot continue because another user exists.
			td.Add(fmt.Sprintf("Email %s already is registered", email))
			tmpl.RenderWith("profile", td, w)
			return
		}
	}

	resp, err := api.UpdateUserInfo(userInfo.Id, email, firstName, lastName, cellphone, address)
	if err != nil {
		td.Add(resp + " - " + err.Error())
		tmpl.RenderWith("profile", td, w)
		return
	}

	//	til here info was updated, so I need recharge info.
	userInfo, err = api.GetUserInfo(email)
	if err != nil {
		td.Add("Error getting user information - " + err.Error())
		tmpl.RenderWith("profile", td, w)
		return
	}

	save("info", userInfo, r)
	saveSession(w, r)

	td.Data["info"] = session.Values["info"]
	td.Info("User information updated")
	tmpl.RenderWith("profile", td, w)
}

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	authenticated := isAuthenticated(r)
	if authenticated {
		redirect(w, r, "/user/profile")
	} else {
		redirect(w, r, "/public/login")
	}
}

func NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	tmpl.Render("notfound", w)
}

//Recover handler.
func CreateRecoverHandler(h http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			r := recover()
			if r != nil {
				log.Printf("An unhandled error ocurred: %v", r)
				tmpl.Render("error", w)
			}
		}()
		h.ServeHTTP(w, r)
	}
}

//Google Handlers

const googleToken = "google-token"

func GoogleLogin(w http.ResponseWriter, r *http.Request) {
	token := google.RandToken()
	save(googleToken, token, r)
	saveSession(w, r)
	redirect(w, r, google.MakeRedirectURL(token))
}

func GoogleCallback(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	token := getGoogleToken(r)

	state := r.FormValue("state")
	if state != token {
		log.Printf("Invalid token state: %s", state)
		tmpl.Render("error", w)
		return
	}

	code := r.FormValue("code")
	accessToken, err := google.AccessToken(code)
	if err != nil {
		log.Printf("Error while getting access token: %s", err.Error())
		tmpl.Render("error", w)
		return
	}

	userInfo, err := google.CreateUserInfo(accessToken)
	if err != nil {
		log.Printf("Error getting userinfo: %s", err.Error())
		tmpl.Render("error", w)
		return
	}

	save("info", userInfo, r)
	save("editable", false, r)
	saveSession(w, r)
	td := td()
	session := getSession(r)
	td.Data["info"] = session.Values["info"]
	td.Data["editable"] = false

	authenticated(w, r)
	redirect(w, r, "/")
}
