package handlers

import (
	"fmt"
	"net/http"
	"user-management-frontend/tmpl"
)

/*Common utilities for handlers*/

func redirect(w http.ResponseWriter, r *http.Request, url string) {
	http.Redirect(w, r, url, http.StatusFound)
}

func td() *tmpl.TemplateData {
	return tmpl.NewTD()
}

func getParam(key string, r *http.Request) string {
	err := r.ParseForm()
	if err != nil {
		//This will render the error page.
		panic(fmt.Sprintf("Cannot parse form when authenticating user: %s", err.Error()))
	}
	value := r.PostForm.Get(key)
	if value == "" {
		value = r.Form.Get(key)
	}
	return value
}

func saveSession(w http.ResponseWriter, r *http.Request) {
	err := getSession(r).Save(r, w)
	if err != nil {
		panic("I can't work without session :(")
	}
}

func getGoogleToken(r *http.Request) string {
	s := getSession(r)
	return s.Values[googleToken].(string)
}

func save(key string, v interface{}, r *http.Request) {
	getSession(r).Values[key] = v
}

func isGoogleLogged(r *http.Request) bool {
	return getSession(r).Values["editable"] != nil && getSession(r).Values["editable"] == false
}
