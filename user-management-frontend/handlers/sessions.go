package handlers

import (
	"net/http"

	"github.com/gorilla/sessions"
)

/*Session management for handlers. Session Store is cookie based*/

var store sessions.Store

const (
	AuthKey = "authenticated"
)

func init() {
	store = sessions.NewCookieStore([]byte("I thing this frontend is much work, but I can do it"))
}

func getSession(r *http.Request) *sessions.Session {
	s, err := store.Get(r, "frontend")
	if err != nil {
		//If this ocurrs, then panic will be recovered ant 500 error page will be rendered
		panic("I cant work without session")
	}
	return s
}

func isAuthenticated(r *http.Request) bool {
	s := getSession(r)
	return s.Values[AuthKey] != nil
}

func invalidateSession(w http.ResponseWriter, r *http.Request) {
	s := getSession(r)
	s.Values = nil
	saveSession(w, r)
}

func authenticated(w http.ResponseWriter, r *http.Request) {
	save(AuthKey, "authenticated", r)
	saveSession(w, r)
}
