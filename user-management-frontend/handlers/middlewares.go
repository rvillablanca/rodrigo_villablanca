package handlers

import (
	"net/http"
)

//AuthenticationMiddleware is use to verify if a user is logged.
func AuthenticationMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//log.Printf("Inside auth middleware for url %s", r.RequestURI)
		s := getSession(r)
		if s.Values[AuthKey] == nil {

			//User not logged, should be redirect to login page
			redirect(w, r, "/public/")
			return
		}

		h.ServeHTTP(w, r)
	})
}

//Add some http headers.
func HeadersMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//log.Printf("Inside headers middleware for url %s", r.RequestURI)
		w.Header().Set("Cache-Control", "no-store, no-cache, must-revalidate")
		h.ServeHTTP(w, r)
	})
}
