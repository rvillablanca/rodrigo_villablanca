package errutil

import "fmt"

func PanicOn(err error) {
	PanicWith(err, "An unrecoverable error ocurred")
}

//PanicWith is used for cases when the operation of a package can't work .
//Example, database access.
func PanicWith(err error, msg string) {
	if err != nil {
		panic(fmt.Sprintf(msg + ": %s", err.Error()))
	}
}
