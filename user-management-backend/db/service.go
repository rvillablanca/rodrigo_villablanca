package db

import (
	"crypto/rand"
	"database/sql"
	"fmt"

	"user-management-backend/mail"

	"log"

	"os"

	"golang.org/x/crypto/bcrypt"
)

type UserService struct {
	db   *sql.DB
	host string
}

//UserService is designed to work as a database layer for all handlers on the backend.
func NewUserService(db *sql.DB) *UserService {
	us := new(UserService)
	us.db = db
	us.host = os.Getenv("FRONTEND_URL")
	if us.host == "" {
		log.Fatal("Cannot get FRONTEND_URL for email templating")
	}
	return us
}

func (us *UserService) NewUser(email, password string) error {

	//An email validation should have here.

	exists, err := us.UserExists(email)
	if err != nil {
		return err
	}

	if exists {
		return UserAlreadyExists(fmt.Sprintf("User %s already exists", email))
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	stringHash := string(hash)
	_, err = us.db.Exec("INSERT INTO USERS (EMAIL, PASSWORD) VALUES (?, ?)", email, stringHash)
	return err
}

func (us *UserService) UpdateUser(userInfo *UserInfo) error {
	_, err := us.db.Exec("UPDATE USERS SET EMAIL = ?, FIRSTNAME = ?, LASTNAME = ?, CELLPHONE = ?, ADDRESS = ? WHERE ID = ?",
		userInfo.Email, userInfo.FirstName, userInfo.LastName, userInfo.Cellphone, userInfo.Address, userInfo.Id)
	if err != nil {
		return err
	}

	return nil
}

//@todo Must improve because this method should be Transactional.
func (us *UserService) ResetPassword(token, password string) error {
	var id int64
	err := us.db.QueryRow("SELECT USER_ID FROM TOKENS WHERE TVALUE = ?", token).Scan(&id)
	if err != nil {
		if err == sql.ErrNoRows {
			return fmt.Errorf("Token %s doesn't exist", token)
		}

		return err
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	_, err = us.db.Exec("UPDATE USERS SET PASSWORD = ? WHERE ID = ?", hash, id)
	if err != nil {
		return err
	}

	_, err = us.db.Exec("DELETE FROM TOKENS WHERE TVALUE = ?", token)
	if err != nil {
		//	In this case I only log the error when the token couldn't be deleted.
		log.Printf("Error where deleting token %s: %s", token, err.Error())
	}

	return nil
}

func (us *UserService) UserExists(email string) (bool, error) {
	var exist bool
	err := us.db.QueryRow("SELECT COUNT(EMAIL) > 0 FROM USERS WHERE EMAIL = ?", email).Scan(&exist)
	if err != nil {
		return false, err
	}
	return exist, nil
}

func (us *UserService) CheckPassword(email, password string) (bool, error) {
	exists, err := us.UserExists(email)
	if err != nil {
		return false, err
	}

	if !exists {
		return false, UserDoesNotExists(fmt.Sprintf("User %s doesn't exists", email))
	}

	var recoveredPass string
	err = us.db.QueryRow("SELECT PASSWORD FROM USERS WHERE EMAIL = ?", email).Scan(&recoveredPass)
	if err != nil {
		return false, err
	}

	err = bcrypt.CompareHashAndPassword([]byte(recoveredPass), []byte(password))
	return err == nil, nil
}

func (us *UserService) CreateToken(email string) error {
	var id int64
	err := us.db.QueryRow("SELECT ID FROM USERS WHERE EMAIL = ?", email).Scan(&id)
	if err != nil {
		return err
	}

	token := token()
	_, err = us.db.Exec("INSERT INTO TOKENS (TVALUE, USER_ID) VALUES (?, ?)", token, id)
	if err != nil {
		return err
	}

	//This could be improved because backend shouldn't know any frontend url, but I don't have time :(
	body := fmt.Sprintf("Recover your password here: %s/public/recovery/%s", us.host, token)
	err = mail.SendMail(email, "Password Recovery", body)
	return err
}

type UserInfo struct {
	Id        int64  `json:"id"`
	Email     string `json:"email"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Cellphone string `json:"cellphone"`
	Address   string `json:"address"`
}

func (us *UserService) FindUserInfo(email string) (*UserInfo, error) {
	var userInfo UserInfo
	err := us.db.QueryRow("SELECT ID, FIRSTNAME, LASTNAME, CELLPHONE, ADDRESS FROM USERS WHERE EMAIL = ?", email).
		Scan(&userInfo.Id, &userInfo.FirstName, &userInfo.LastName, &userInfo.Cellphone, &userInfo.Address)
	if err != nil {
		return nil, err
	}

	userInfo.Email = email
	return &userInfo, nil
}

func token() string {
	b := make([]byte, 20)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}
