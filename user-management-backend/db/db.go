package db

import (
	"database/sql"
	// Mysql driver
	_ "github.com/go-sql-driver/mysql"
)

//Database initialization.
func InitializeDatabase(url string) (*sql.DB, error) {
	db, err := sql.Open("mysql", url)
	if err != nil {
		return nil, err
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	db.SetMaxIdleConns(5)
	db.SetMaxOpenConns(10)
	return db, nil
}
