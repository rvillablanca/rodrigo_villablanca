package db

/*
https://dave.cheney.net/2014/12/24/inspecting-errors
https://dave.cheney.net/2016/04/27/dont-just-check-errors-handle-them-gracefully
*/

//Specific error types that should be handled for specific cases.
type UserAlreadyExists string
type UserDoesNotExists string

func (u UserAlreadyExists) Error() string {
	return string(u)
}

func (u UserDoesNotExists) Error() string {
	return string(u)
}

func IsUserAlreadyExists(err error) bool {
	_, ok := err.(UserAlreadyExists)
	return ok
}

func IsUserDoesNotExists(err error) bool {
	_, ok := err.(UserDoesNotExists)
	return ok
}
