package mail

import "net/smtp"

//From https://gist.github.com/jpillora/cb46d183eca0710d909a
func SendMail(to, subject, body string) error {
	from := "kdutesting@gmail.com"

	msg := "From: " + from + "\n" +
		"To: " + to + "\n" +
		"Subject: " + subject + "\n\n" +
		body

	//	Plain user/pass here, it doesn't matter because it is only a testing account.
	err := smtp.SendMail("smtp.gmail.com:587",
		smtp.PlainAuth("", from, "changeit10", "smtp.gmail.com"),
		from, []string{to}, []byte(msg))
	if err != nil {
		return err
	}

	return nil
}
