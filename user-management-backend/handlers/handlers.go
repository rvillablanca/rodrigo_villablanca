package handlers

import (
	"net/http"
	"user-management-backend/db"

	"github.com/gorilla/mux"

	"encoding/json"
	"fmt"
)

func CreateUserHandler(w http.ResponseWriter, r *http.Request) {
	user := struct {
		Email string `json:"email"`
		Pass  string `json:"pass"`
	}{}

	json.NewDecoder(r.Body).Decode(&user)

	err := UserService.NewUser(user.Email, user.Pass)
	if err != nil {

		if db.IsUserAlreadyExists(err) {
			requestFailed(err.Error(), w)
			return
		}

		error(fmt.Sprintf("Error when creating user %s", user.Email), w)
		return
	}

	success(fmt.Sprintf("User %s created", user.Email), w)
}

func UpdateUserHandler(w http.ResponseWriter, r *http.Request) {
	userInfo := db.UserInfo{}

	json.NewDecoder(r.Body).Decode(&userInfo)

	err := UserService.UpdateUser(&userInfo)
	if err != nil {
		requestFailed(err.Error(), w)
		return
	}

	success(fmt.Sprintf("Information updated successfully for user %s - %d", userInfo.Email, userInfo.Id), w)
}

func ResetPasswordHandler(w http.ResponseWriter, r *http.Request) {
	data := struct {
		Token string `json:"token"`
		Pass  string `json:"pass"`
	}{}

	json.NewDecoder(r.Body).Decode(&data)

	if data.Token == "" {
		badRequest("Missing token value", w)
		return
	}

	if data.Pass == "" {
		badRequest("Missing password value", w)
		return
	}

	err := UserService.ResetPassword(data.Token, data.Pass)
	if err != nil {
		requestFailed(err.Error(), w)
		return
	}

	success("Password updated successfully", w)
}

func AuthenticateUserHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	email := vars["email"]
	valid, err := UserService.CheckPassword(email, vars["pass"])
	if err != nil {

		if db.IsUserDoesNotExists(err) {

			//	Failed response with 400 status code because any client should query existence of user before try to login.
			badRequest(fmt.Sprintf("User %s does not exists", email), w)
			return
		}

		error(fmt.Sprintf("Error when checking credentials for user %s", email), w)
		return
	}

	if valid {
		success(fmt.Sprintf("User %s authenticated successfully", email), w)
	} else {
		requestFailed("Authentication failed", w)
	}
}

func UserExistsHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	email := vars["email"]
	exist, err := UserService.UserExists(email)
	if err != nil {
		error(fmt.Sprintf("An error has ocurred when verifiyng user existence for %s: %s", email, err.Error()), w)
		return
	}

	if exist {
		success(fmt.Sprintf("User %s exists", email), w)
	} else {
		requestFailed(fmt.Sprintf("User %s does not exists", email), w)
	}
}

func GetUserInfoHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	email := vars["email"]

	exist, err := UserService.UserExists(email)
	if err != nil {
		error(fmt.Sprintf("An error has ocurred when verifiyng user existence for %s: %s", email, err.Error()), w)
		return
	}

	if !exist {
		requestFailed(fmt.Sprintf("User %s does not exists", email), w)
		return
	}

	userInfo, err := UserService.FindUserInfo(email)
	if err != nil {
		requestFailed(fmt.Sprintf("Error finding user information for %s: %s", email, err.Error()), w)
		return
	}

	writeJson(userInfo, w)
}

func CreateTokenHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	email := vars["email"]

	exist, err := UserService.UserExists(email)
	if err != nil {
		error(fmt.Sprintf("An error has ocurred when verifiyng user existence for %s: %s", email, err.Error()), w)
		return
	}

	if !exist {
		requestFailed(fmt.Sprintf("User %s does not exists", email), w)
		return
	}

	err = UserService.CreateToken(email)
	if err != nil {
		requestFailed(fmt.Sprintf("Error creating token for email %s: %s", email, err.Error()), w)
		return
	}

	success(fmt.Sprintf("Token generated for user %s", email), w)
}

func CreateRecoverHandler(h http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			r := recover()
			if r != nil {
				//	Error will be sent to frontend (or other client), but frontend shouldn't show the error message to final client (user).
				error(fmt.Sprintf("An unhandled error ocurred while processing request: %v", r), w)
			}
		}()
		h.ServeHTTP(w, r)
	}
}

//Custom not found handler
func NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	failed(http.StatusNotFound, "The resource was not found", w)
}
