package handlers

import (
	"encoding/json"
	"net/http"
	"user-management-backend/db"
)

// UserService is the global reference for UserService
var UserService *db.UserService

type basicResponse struct {
	Message string `json:"message"`
}

/*
HTTP Error Codes

200 for success
400 for missing, required parameters or general bad request from client.

402 Valid parameters values but the request failed (Based on string api response codes https://stripe.com/docs/api#errors).
Maybe a best approach would be send an boolean status on the body response indicating request status with a 200 http code.
I choose the first according remaining time.

404 for not found resources
500 for server errors
*/

func error(msg string, w http.ResponseWriter) {
	failed(http.StatusInternalServerError, msg, w)
}

func badRequest(msg string, w http.ResponseWriter) {
	failed(http.StatusBadRequest, msg, w)
}

func failed(code int, msg string, w http.ResponseWriter) {
	w.WriteHeader(code)
	writeJson(&basicResponse{Message: msg}, w)
}

func success(msg string, w http.ResponseWriter) {
	//Implicit 200 on first write
	writeJson(&basicResponse{Message: msg}, w)
}

func requestFailed(msg string, w http.ResponseWriter) {
	failed(http.StatusPaymentRequired, msg, w)
}

func writeJson(v interface{}, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(v)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
}
